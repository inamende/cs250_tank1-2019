/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
Purpose: Implement a graphics pipeline.
Language: C++
Platform: Windows 7, Visual Studio DEBUG X86
Project: Assignment 2 tank
Author: Ignacio Mendezona, ignacio.mendezona
----------------------------------------------------------------------------------------------------------*/

Controls: