/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: CS250Parser.h
Purpose: Implement the functions from CS250Parser.h
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Jon Sanchez
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#pragma once

#include <vector>
#include "Point4.h"

class CS250Parser
{
	public:
		static void LoadDataFromFile();

		struct Face
		{
			int indices[3];
		};

		static float left;
		static float right;
		static float top;
		static float bottom;
		static float focal;
		static float nearPlane;
		static float farPlane;

		static std::vector<Point4> vertices;
		static std::vector<Face> faces;
		static std::vector<Point4> colors;
		static std::vector<Point4> textureCoords;

};