#include "Utilities.h"

sf::Image FrameBuffer::image{};
int FrameBuffer::width = WIDTH;
int FrameBuffer::height = HEIGHT;

FrameBuffer::FrameBuffer()
{
}

FrameBuffer::~FrameBuffer()
{
}

//Clears the fram buffer to the given color
void FrameBuffer::Clear(sf::Color color)
{
	for (unsigned x = 0; x < WIDTH; x++)
		for (unsigned y = 0; y < HEIGHT; y++)
			image.setPixel(x, y, color);
}


//Set the pixel's color at (x,y)
void FrameBuffer::SetPixel(const int &x, const int &y, const unsigned char &r, const unsigned char &g, const unsigned char &b)
{
	// we dont want pixels out of the screen
	if (x < 0 || x >= width || y < 0 || y >= height)
		return;

	const sf::Color color{ r, g, b};

	image.setPixel(x, y, color);
}

//Stores the color of the given (x,y) pixel in r, g & b
void FrameBuffer::GetPixel(const int &x, const int &y, unsigned char &r, unsigned char &g, unsigned char &b)
{
	// probably need to change this in the future

	int _y = height - y;

	// we dont want pixels out of the screen
	if (x < 0 || x >= width || _y < 0 || _y >= height)
		return;
}