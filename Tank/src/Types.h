#ifndef TYPES_H
#define TYPES_H

// Typedefs for consistency
typedef unsigned char      u8;
typedef char               s8;
typedef unsigned int       u32;
typedef int                s32;
//typedef unsigned long long u64;
//typedef long long          s64;
typedef float              f32;
typedef double             f64;

#endif
