/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: DrawLine.h
Purpose: Model class that is able to draw a model either with lines or with triangles
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#pragma once
#include "DrawTriangle.h"
#include "DrawLine.h"
#include <vector>
class Model
{
public:
	Point4 translation_{ 0, 0, 0 };
	Point4 rotation_{0,0,0};
	Point4 scale_{ 1, 1, 1 };
	void Draw();

	Model* parent = nullptr;
	static bool renderLines;

private:
	std::vector<Vertex>vertices;
};