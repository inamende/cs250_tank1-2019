/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: main.cpp
Purpose: All the "gameplay" code
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#include "Utilities.h"
#include "Rasterizer.h"
#include "DrawTriangle.h"
#include "DrawLine.h"
#include "CS250Parser.h"
#include "Model.h"
#include "Matrix4.h"
#include <iostream>

#include <SFML/Graphics.hpp>

sf::Texture texture;
sf::Sprite sprite;

//Creation of the parts of the tank
Model body{};
Model wheels[4];
Model turret{};
Model gun{};

void input(sf::RenderWindow & window)
{
	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Escape))
	{
		window.close();
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::D))
	{
		// body left
		body.rotation_.y += 0.1f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::A))
	{
		//body right
		body.rotation_.y -= 0.1f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Space))
	{
		// move forward
		body.translation_.z += cos(body.rotation_.y);
		body.translation_.x += sin(body.rotation_.y);

		//Wheel rotation
		for (size_t i = 0; i < 4; i++)
			wheels[i].rotation_.x += 0.2f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Q))
	{
		// turret left
		turret.rotation_.y -= 0.1f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::E))
	{
		//turret righ 
		turret.rotation_.y += 0.1f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::R))
	{
		// gun up
		gun.rotation_.x += 0.1f;
		gun.translation_.y = -sin(gun.rotation_.x) * (gun.scale_.z / 2);
		gun.translation_.z = cos(gun.rotation_.x) * (gun.scale_.z / 2) + 12.5f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::F))
	{
		// gun down
		gun.rotation_.x -= 0.1f;
		gun.translation_.y = -sin(gun.rotation_.x) * (gun.scale_.z / 2);
		gun.translation_.z = cos(gun.rotation_.x) * (gun.scale_.z / 2) + 12.5f;
	}

	if (sf::Keyboard::isKeyPressed(sf::Keyboard::Num2))
	{
		// wire frame mode?
		Model::renderLines = !Model::renderLines;
	}
}

int x, y = 0;

void render(sf::RenderWindow & window)
{
	FrameBuffer::Clear(sf::Color::White);

	body.Draw();

	for (size_t i = 0; i < 4; i++)
		wheels[i].Draw();

	turret.Draw();
	gun.Draw();

	texture.update(FrameBuffer::image);
	sprite.setTexture(texture);
	window.draw(sprite);
	window.display();
}

void init(void)
{
	//Initialize everything here
	CS250Parser::LoadDataFromFile();

	//body setup
	body.translation_.z = -100;
	body.scale_ = { 30,25,80 };

	//wheels setup
	wheels[0].translation_ = { 17.5, -12.5, -25 };
	wheels[1].translation_ = { -17.5, -12.5, -25 };
	wheels[2].translation_ = { 17.5, -12.5, 25 };
	wheels[3].translation_ = { -17.5, -12.5, 25 };

	for (size_t i = 0; i < 4; i++)
	{
		wheels[i].scale_ = { 5,20,20 };
		wheels[i].parent = &body;
	}

	//turret setup
	turret.translation_ = { 0,20,0 };
	turret.scale_ = { 25,15,25 };
	turret.parent = &body;

	//gun setup
	gun.translation_ = { 0,0,32.5 };
	gun.scale_ = { 5,5,40 };
	gun.parent = &turret;
}

int main()
{
	sf::RenderWindow window(sf::VideoMode(WIDTH, HEIGHT), "CS250 TANK");

	texture.create(WIDTH, HEIGHT);
	FrameBuffer::image.create(WIDTH, HEIGHT, sf::Color::White);

	init();

	while (window.isOpen())
	{
		sf::Event event;
		while (window.pollEvent(event))
		{
			if (event.type == sf::Event::Closed)
			{
				window.close();
			}
		}

		// update input, render..
		input(window);
		render(window);
	}

	return 0;
}