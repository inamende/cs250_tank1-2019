/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: DrawTriangle.h
Purpose: This file only has a function to Draw a triangle, and a vertex structure.
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/

#pragma once

#include "Color.h"
#include "Point4.h"
	/// -----------------------------------------------------------------------
	/// \class	Vertex
	///	\brief	Simple structure used to define a triangle vertex with both position
	///			and color attribute. 
	struct Vertex
	{
		Vertex() = default;
		Vertex(float x, float y, float z, Color c);
		Vertex(Point4, Color);

		Point4  position_;	// x, y, z
		Color	color_;		// r,g,b,a
	};

	/// -----------------------------------------------------------------------
	/// \fn		DrawTriangleBarycentric
	/// \brief	Rasterizes a CCW triangle defined by p1, p2, p3, using the incremental 
	///			barycentric coordinates method described in class for traversal and color
	///			interpolation.
	/// \param	p1	First triangle vertex (position/color).
	///	\param	p2	Second triangle vertex (position/color).
	///	\param	p3	Third triangle vertex (position/color).
	void DrawTriangle(const Point4 &, const Point4 &, const Point4 &,const Color& );