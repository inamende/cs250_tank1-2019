/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: Color.cpp
Purpose: Implement the functions from Color.h
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/


#include "Color.h"
#include "Point4.h"

	// ------------------------------------------------------------------------
	/// \fn		Default & custom
	/// \brief	
	Color::Color()
		: r(0.0f)
		, g(0.0f)
		, b(0.0f)
		, a(1.0f)
	{}

	// ------------------------------------------------------------------------
	/// \fn		Default & custom
	/// \brief	
	Color::Color(float rr, float gg, float bb, float aa)
		: r(rr)
		, g(gg)
		, b(bb)
		, a(aa)
	{}

	// ------------------------------------------------------------------------
	/// \fn		custom constructor
	/// \brief
	Color::Color(unsigned color) // custom constructor
	{
		// decompose
		unsigned aa = (color & (0x000000FF << 24)) >> 24;
		unsigned rr = (color & (0x000000FF << 16)) >> 16;
		unsigned gg = (color & (0x000000FF << 8)) >> 8;
		unsigned bb = color & 0x000000FF;

		// float
		r = (float)rr / 255.0f;
		g = (float)gg / 255.0f;
		b = (float)bb / 255.0f;
		a = (float)aa / 255.0f;
	}

	// ------------------------------------------------------------------------
	/// \fn		conversion operator
	/// \brief
	Color::operator unsigned()	// conversion operator
	{
		// convert each element to range [0,255]
		unsigned aa = (a < 0.0f) ? (0) : ((a > 1.0f) ? (255) : unsigned(a*255.0f));
		unsigned rr = (r < 0.0f) ? (0) : ((r > 1.0f) ? (255) : unsigned(r*255.0f));
		unsigned gg = (g < 0.0f) ? (0) : ((g > 1.0f) ? (255) : unsigned(g*255.0f));
		unsigned bb = (b < 0.0f) ? (0) : ((b > 1.0f) ? (255) : unsigned(b*255.0f));

		// pack
		unsigned c = (aa << 24) | (rr << 16) | (gg << 8) | bb;
		return c;
	}

	Color Color::operator *(const float & sc)const { return Color(r*sc, g*sc, b*sc, a*sc); }
	void Color::operator *=(const float & sc) { *this = *this * sc; }
	Color Color::operator +(const Color & rhs)const  {
		return Color(
			r + rhs.r,
			g + rhs.g,
			b + rhs.b,
			a + rhs.a);
	}
	void Color::operator += (const Color & rhs)
	{
		*this = *this + rhs;
	}

	//Conversion from point to color
	Color & Color::operator=(Point4 p)
	{
		r = p.x;
		g = p.y;
		b = p.z;
		a = p.w;

		return *this;
	}

