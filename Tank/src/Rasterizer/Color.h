/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: CS250Parser
Purpose: Basic Color operations.
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#pragma once
#pragma warning (default:4201) // nameless struct warning

class Point4;

	struct Color
	{
		union
		{
			struct
			{
				float r, g, b, a;
			};
			float v[4];
		};

		Color();
		Color(float rr, float gg, float bb, float aa = 1.0f);
		Color(unsigned color); // custom constructor
		Color operator *(const float & sc) const;
		void operator *=(const float & sc);
		Color operator +(const Color & rhs) const;
		void operator += (const Color & rhs);
		Color& operator=(Point4);
		operator unsigned();	// conversion operator
	};