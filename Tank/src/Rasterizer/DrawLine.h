/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: DrawLine.h
Purpose: This file only has a function to Draw a line. Doesn't matter what kind of line.
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#pragma once

#include "Color.h"
#include "Point4.h"

void DrawLine(const Point4 &p1, const Point4 &p2, const Color & c);