#ifndef CS200_RASTERIZER_H_
#define CS200_RASTERIZER_H_

// forward declaration of aevec2
//struct AEVec2;

// Provided Framework

#include "Color.h"			// Color
#include "Utilities.h"	// Frame buffer

// - Assignment Includes

#include "DrawTriangle.h"	// Assignment 3
#endif