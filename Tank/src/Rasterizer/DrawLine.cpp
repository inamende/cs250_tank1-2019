/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: DrawLine.cpp
Purpose: Implement the functions from DrawLIne.h
Language: C++
Platform: Windows7
Project: Assingment 2, Tank (Tanken from cs200)
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#include "Point4.h"
#include "Rasterizer.h"
#include "cmath" //absolute value
#include "DrawLine.h"

// ------------------------------------------------------------------------
/// \fn		DrawLineBresenham
/// \brief	Draws a line using the Bresenham algorithm presented in class.
/// \param  p1, p2 and a color c
/// \output void
void DrawLine(const Point4 &p1, const Point4 &p2, const Color & c)
{
	f32 deltaX = p2.x - p1.x; //increment of X
	f32 deltaY = p2.y - p1.y; //increment of Y

	f32 m = deltaY / deltaX; //slope

	/*if ((int)deltaX == 0)
	{
		DrawHorizontalLine(p1, p2, c);
		return;
	}

	if ((int)deltaY == 0)
	{
		DrawVerticalLine(p1, p2, c);
		return;
	}*/

	if (abs(m) < 1)
	{
		int xStep = (deltaX < 0) ? -1 : 1; //To know if we need to draw it from right to left or from left to right
		f32 yStep = (deltaY < 0) ? -abs(m) : abs(m);//To know if we need to draw it upwards or downwards
		int x = (int)roundf(p1.x);//the starting position of x
		f32 y = p1.y;//the starting position of y

		deltaX = abs(deltaX);//to keep it positive, since we will be substracting 1 to it in every loop

		do
		{
			FrameBuffer::SetPixel(x, (int)roundf(y), (char)c.r, (char)c.g, (char)c.b);
			x += xStep;//To know if we need to draw it from right to left or from left to right
			y += yStep;//To know if we need to draw it upwards or downwards
		} while (--deltaX >= 0);//here we know if we have something else to draw. When deltaX gets to 0, we finished
	}
	else //abs(m) > 1
	{
		int y = (int)roundf(p1.y);//the starting position of y
		f32 x = p1.x;//the starting position of x
		int yStep = (deltaY < 0) ? -1 : 1; //To know if we need to draw it upwards or downwards
		f32 xStep = (deltaX < 0) ? -(abs(1 / m)) : abs(1 / m); //To know if we need to draw it from right to left or from left to right

		deltaY = abs(deltaY);//to keep it positive, since we will be substracting 1 to it in every loop

		do
		{
			FrameBuffer::SetPixel((int)roundf(x), y, (char)c.r, (char)c.g, (char)c.b);
			x += xStep;//To know if we need to draw it from right to left or from left to right
			y += yStep;//To know if we need to draw it upwards or downwards
		} while (--deltaY >= 0);//here we know if we have something else to draw. When deltaY gets to 0, we finished
	}
}

void DrawHorizontalLine(const Point4 &p1, const Point4 &p2, const Color & c)
{
	int deltaX = (int)roundf(p2.x - p1.x);//to know how long is the line on the X axis
	int x = (int)roundf(p1.x);//the starting position of x
	int y = (int)roundf(p1.y);//the y position
	int xStep = (deltaX < 0) ? -1 : 1;//To know if we need to draw it from right to left or from left to right
	deltaX = (deltaX < 0) ? -deltaX : deltaX; //just to be according to the xStep

	do
	{
		FrameBuffer::SetPixel(x, y, (char)c.r, (char)c.g, (char)c.b);
		x += xStep;//upadte x to draw the next pixel
	} while (--deltaX >= 0);
}

void DrawVerticalLine(const Point4 & p1, const Point4 & p2, const Color & c)
{
	int deltaY = (int)roundf(p2.y - p1.y);//to know how long is the line on the Y axis
	int y = (int)roundf(p1.y);//the starting position of y
	int x = (int)roundf(p1.x);//the starting position of x
	int yStep = (deltaY < 0) ? -1 : 1;//To know if we need to draw it from right to left or from left to right
	deltaY = abs(deltaY);//to keep it positive, since we will be substracting 1 to it in every loop

	while (--deltaY >= 0)
	{
		FrameBuffer::SetPixel(x, y, (char)c.r, (char)c.g, (char)c.b);
		y += yStep;//update y to drw the next pixel
	} ;//here we know if we have something else to draw. When deltaY gets to 0, we finished
}
