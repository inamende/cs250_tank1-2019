/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: DrawTriangle.cpp
Purpose: Implemetn functions from DrawTriangle.h
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#include "Rasterizer.h"
#include <iostream>

void DetermineCase(const Point4 &v0, const Point4 &v1, const Point4 &v2, int & top, int & mid, int & bot, bool & midIsLeft)
{
	if (v0.y > v1.y) // case 1, 2, 3
	{
		if (v0.y < v2.y) // case 3
		{
			top = 2; // top
			mid = 0; // mid
			bot = 1; // bot
			midIsLeft = true;
		}
		else //case 1 and 2
		{
			top = 0;
			
			if (v1.y < v2.y) // case 2
			{
				mid = 2;
				bot = 1;
				midIsLeft = false;
			}
			else // case 1
			{
				mid = 1;
				bot = 2;
				midIsLeft = true;
			}
		}
	}
	else // case 4, 5, 6
	{
		if (v2.y > v1.y) // case 6
		{
			top = 2;
			mid = 1;
			bot = 0;
			midIsLeft = false;
		}
		else // case 4, 5
		{
			top = 1;

			if (v0.y > v2.y) // case 4
			{
				mid = 0;
				bot = 2;
				midIsLeft = false;
			}
			else // case 5
			{
				mid = 2;
				bot = 0;
				midIsLeft = true;
			}
		}
	}
}

/// -----------------------------------------------------------------------
/// \fn		DrawTriangle
/// \brief	Rasterizes a CCW triangle defined by p1, p2, p3 using the naive 
///			algorithm described in class. Each pixel in the triangle share
///			the same color.
/// \param	p1	First triangle vertex position.
///	\param	p2	Second triangle vertex position.
///	\param	p3	Third triangle vertex position.
///	\param	c	Color to fill the triangle with.
void DrawTriangle(const Point4 &v0, const Point4 &v1, const Point4 &v2,const Color & c)
{
	// SETUP
	Point4 triangleVertex[3]; // Array of vertexes
	triangleVertex[0] = v0;
	triangleVertex[1] = v1;
	triangleVertex[2] = v2;

	// Indexes to acces the triangleVertex array
	int top;
	int mid;
	int bot;
	bool midIsLeft;

	//Give values to midisleft and top mid bot indexes in such a way that when triangleVertex[top] returns the higher vertex. Same with mid and top.
	DetermineCase(v0, v1, v2, top, mid, bot, midIsLeft);

	// invSlope for every case
	float invSlope_TOP_BOT = (triangleVertex[bot].x - triangleVertex[top].x) / (triangleVertex[bot].y - triangleVertex[top].y);
	float invSlope_TOP_MID = (triangleVertex[mid].x - triangleVertex[top].x) / (triangleVertex[mid].y - triangleVertex[top].y);
	float invSlope_MID_BOT = (triangleVertex[bot].x - triangleVertex[mid].x) / (triangleVertex[bot].y - triangleVertex[mid].y);

	float invSlopes[3] = {invSlope_TOP_BOT, invSlope_TOP_MID, invSlope_MID_BOT};
	int leftIndx = midIsLeft;
	int rightIndx = !midIsLeft;
	
	//Left and right most pixels on a certain y
	f32 xL = triangleVertex[top].x;
	f32 xR = xL;

	//1st half
	for (int y = static_cast<int>(std::ceilf(triangleVertex[top].y)); y > static_cast<int>(std::ceilf(triangleVertex[mid].y)); y--)
	{
		for (int x = static_cast<int>(std::floorf(xL)); x < static_cast<int>(std::floorf(xR)); x++)
			FrameBuffer::SetPixel(x, y, (char)c.r, (char)c.g, (char)c.b);

		xL -= invSlopes[leftIndx];
		xR -= invSlopes[rightIndx];
	}

	//Update the indexes
	leftIndx = midIsLeft << 1;
	rightIndx = !midIsLeft << 1;

	//Update the Left or Right pixel
	if (midIsLeft)
		xL = (triangleVertex[mid].x); 
	else
		xR = (triangleVertex[mid].x);

	//2nd half
	for (int y = static_cast<int>(std::ceilf(triangleVertex[mid].y)); y > static_cast<int>(std::ceilf(triangleVertex[bot].y)); y--)
	{
		for (int x = static_cast<int>(std::floorf(xL)); x < static_cast<int>(std::floorf(xR)); x++)
			FrameBuffer::SetPixel(x, y, (char)c.r, (char)c.g, (char)c.b);

		xL -= (invSlopes[leftIndx]);
		xR -= (invSlopes[rightIndx]);
	}
}