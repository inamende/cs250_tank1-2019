#pragma once

#include "windows.h"
#include "math.h"
#include "stdio.h"

#include <SFML/Graphics.hpp>

#define WIDTH 1024
#define HEIGHT 728

#define PI 3.14159265358979323846
#define DEG_TO_RAD (PI / 180.0)

class FrameBuffer
{
	public:
		FrameBuffer();
		~FrameBuffer();

		static void Clear(sf::Color color);

		//static void Init(sf::Image & image);
		static void SetPixel(const int &x, const int &y, const unsigned char &r, const unsigned char &g, const unsigned char &b);
		static void GetPixel(const int &x, const int &y, unsigned char &r, unsigned char &g, unsigned char &b);

	public:
		static sf::Image image; // same as frame buffer
		static int width;
		static int height;
};