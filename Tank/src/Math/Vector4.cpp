/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: Vector.cpp
Purpose: Math Library
Language: C++
Platform: PC, Windows 7
Project: ignacio.mendezona@digipen.edu ,cs250 Math Library
class CS250
Author: Ignacio Mendezona , ignacio.mendezona@digipen.edu
Date: Wed Jan 24 11:58:18 2018
----------------------------------------------------------------------------------------------------------*/


#include "Vector4.h"
#include "MathUtilities.h"
#include <cmath>

/**************************************************************************
*!
\fn     Vector4::Vector4

\brief Default constructor, initializes x,y,z to zeroes, w to defined value


\param  void


*
**************************************************************************/
Vector4::Vector4(void)
{
	Zero();
}

/**************************************************************************
*!
\fn     Vector4::Vector4

\brief Copy constructor, copies every component from the other Vector4


\param  const Vector4 & rhs


*
**************************************************************************/
Vector4::Vector4(const Vector4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] = rhs.v[i];
}

/**************************************************************************
*!
\fn     Vector4::Vector4

\brief Non-Default constructor, self explanatory


\param  float xx ...


*
**************************************************************************/
Vector4::Vector4(float xx, float yy, float zz, float ww)
{
	v[0] = xx;
	v[1] = yy;
	v[2] = zz;
	v[3] = ww;
}

/**************************************************************************
*!
\fn     Vector4::operator=

\brief Assignment operator, does not need to handle self assignment


\param  const Vector4 & rhs


\return Vector4 &


*
**************************************************************************/
Vector4 & Vector4::operator=(const Vector4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] = rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Vector4::operator-

\brief Unary negation operator, negates all components and returns a copy


\param  void


\return Vector4


*
**************************************************************************/
Vector4 Vector4::operator-(void) const
{
	Vector4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = -v[i];

	return result;
}

/**************************************************************************
*!
\fn     Vector4::operator+

\brief Add Vector to Vector B


\param  const Vector4 & rhs


\return Vector4


*
**************************************************************************/
Vector4 Vector4::operator+(const Vector4 & rhs) const
{
	Vector4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = v[i] + rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Vector4::operator-

\brief Subtract Vector A from vector B


\param  const Vector4 & rhs


\return Vector4


*
**************************************************************************/
Vector4 Vector4::operator-(const Vector4 & rhs) const
{
	Vector4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = v[i] - rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Vector4::operator*

\brief multiply a vector with a scalar


\param  const float rhs


\return Vector4


*
**************************************************************************/
Vector4 Vector4::operator*(const float rhs) const
{
	Vector4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = v[i] * rhs;

	return result;
}

/**************************************************************************
*!
\fn     Vector4::operator/

\brief divide a vector by that scalar


\param  const float rhs


\return Vector4


*
**************************************************************************/
Vector4 Vector4::operator/(const float rhs) const
{
	Vector4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = v[i] / rhs;

	return result;
}

/**************************************************************************
*!
\fn     Vector4::operator+=

\brief Add Vector to Vector B, stores the result in the original vector


\param  const Vector4 & rhs


\return Vector4 &


*
**************************************************************************/
Vector4 & Vector4::operator+=(const Vector4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] += rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Vector4::operator-=

\brief Substract Vector to Vector Bstores the result in the original vector


\param  const Vector4 & rhs


\return Vector4 &


*
**************************************************************************/
Vector4 & Vector4::operator-=(const Vector4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] -= rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Vector4::operator*=

\brief multiply a vector with a scalar, stores the result in the original vector


\param  const float rhs


\return Vector4 &


*
**************************************************************************/
Vector4 & Vector4::operator*=(const float rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] *= rhs;

	return *this;
}

/**************************************************************************
*!
\fn     Vector4::operator/=

\brief divide a vector with a scalar, stores the result in the original vector


\param  const float rhs


\return Vector4 &


*
**************************************************************************/
Vector4 & Vector4::operator/=(const float rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] /= rhs;

	return *this;
}

/**************************************************************************
*!
\fn     Vector4::operator==

\brief True if two vectors are equal, false if not


\param  const Vector4 & rhs


\return bool


*
**************************************************************************/
bool Vector4::operator==(const Vector4 & rhs) const
{
	for (int i = 0; i < 4; i++)
		if (!isEqual(v[i], rhs.v[i]))
			return false;

	return true;
}

/**************************************************************************
*!
\fn     Vector4::operator!=

\brief False if two vectors are equal, true if not


\param  const Vector4 & rhs


\return bool


*
**************************************************************************/
bool Vector4::operator!=(const Vector4 & rhs) const
{
	for (int i = 0; i < 3; i++)
		if (!isEqual(v[i], rhs.v[i]))
			return true;

	return false;
}

/**************************************************************************
*!
\fn     Vector4::Dot

\brief Dot product


\param  const Vector4 & rhs


\return float


*
**************************************************************************/
float Vector4::Dot(const Vector4 & rhs) const
{
	float result = 0;

	for (int i = 0; i < 3; i++)
		result += v[i] * rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Vector4::Cross

\brief Cross Product


\param  const Vector4 & rhs


\return Vector4


*
**************************************************************************/
Vector4 Vector4::Cross(const Vector4 & rhs) const
{
	Vector4 result;
	result.v[0] = v[1]* rhs.v[2] - rhs.v[1] * v[2];
	result.v[1] = v[2] * rhs.v[0] - rhs.v[2] * v[0];
	result.v[2] = v[0] * rhs.v[1] - rhs.v[0] * v[1];
	result.v[3] = 0.0f;

	return result;
}

/**************************************************************************
*!
\fn     Vector4::Length

\brief Computes the true length of the vector


\param  void


\return float


*
**************************************************************************/
float Vector4::Length(void) const
{
	float result = std::sqrt(v[0] * v[0] + v[1] * v[1] + v[2] * v[2] + v[3] * v[3]);

	return result;
}

/**************************************************************************
*!
\fn     Vector4::LengthSq

\brief Computes the squared length of the vector


\param  void


\return float


*
**************************************************************************/
float Vector4::LengthSq(void) const
{
	return Length() * Length();
}

/**************************************************************************
*!
\fn     Vector4::Normalize

\brief Normalizes the vector to make the final vector be of length 1. If the length is zero
       then this function should not modify anything.


\param  void


*
**************************************************************************/
void Vector4::Normalize(void)
{
	if (Length() == 0.0f)
		return;
	*this = *this / Length();
}

/**************************************************************************
*!
\fn     Vector4::Zero

\brief Sets x,y,z to zeroes, w to defined value


\param  void


*
**************************************************************************/
void Vector4::Zero(void)
{
	for (int i = 0; i < 4; i++)
		v[i] = 0.0f;
}
