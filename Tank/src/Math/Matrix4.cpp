/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: Matrix.cpp
Purpose: Math Library
Language: C++
Platform: PC, Windows 7
Project: ignacio.mendezona@digipen.edu ,cs250 Math Library
class CS250
Author: Ignacio Mendezona , ignacio.mendezona@digipen.edu
Date: Wed Jan 24 11:58:18 2018
----------------------------------------------------------------------------------------------------------*/


#include "Matrix4.h"
#include "MathUtilities.h"
#include <cmath>

/**************************************************************************
*!
\fn     Matrix4::Matrix4

\brief Default constructor should initialize to zeroes


\param  void


*
**************************************************************************/
Matrix4::Matrix4(void)
{
	Zero();
}

/**************************************************************************
*!
\fn     Matrix4::Matrix4

\brief Copy constructor, copies every entry from the other matrix.


\param  const Matrix4 & rhs


*
**************************************************************************/
Matrix4::Matrix4(const Matrix4 & rhs)
{
	for (int i = 0; i < 16; i++)
		v[i] = rhs.v[i];
}

/**************************************************************************
*!
\fn     Matrix4::Matrix4

\brief Non-default constructor, self-explanatory


\param  float mm


*
**************************************************************************/
Matrix4::Matrix4(float mm00, float mm01, float mm02, float mm03, float mm10, float mm11, float mm12, float mm13, 
				 float mm20, float mm21, float mm22, float mm23, float mm30, float mm31, float mm32, float mm33)
{
	m[0][0] = mm00; m[0][1] = mm01; m[0][2] = mm02; m[0][3] = mm03;
	m[1][0] = mm10; m[1][1] = mm11; m[1][2] = mm12; m[1][3] = mm13;
	m[2][0] = mm20; m[2][1] = mm21; m[2][2] = mm22; m[2][3] = mm23;
	m[3][0] = mm30; m[3][1] = mm31; m[3][2] = mm32; m[3][3] = mm33;
}

/**************************************************************************
*!
\fn     Matrix4::operator=

\brief Assignment operator, does not need to handle self-assignment


\param  const Matrix4 & rhs


\return Matrix4 &


*
**************************************************************************/
Matrix4 & Matrix4::operator=(const Matrix4 & rhs)
{
	for (int i = 0; i < 16; i++)
		v[i] = rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Matrix4::operator*

\brief Multiplying a Matrix4 with a Vector4


\param  const Vector4 & rhs


\return Vector4


*
**************************************************************************/
Vector4 Matrix4::operator*(const Vector4 & rhs) const
{
	Vector4 result;

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
			result.v[i] += m[i][j] * rhs.v[j];
	}

	return result;
}

/**************************************************************************
*!
\fn     Matrix4::operator*

\brief  Multiplying a Matrix4 with a Point4


\param  const Point4 & rhs


\return Point4


*
**************************************************************************/
Point4 Matrix4::operator*(const Point4 & rhs) const
{
	Point4 result(0, 0, 0, 0);

	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++)
			result.v[i] += m[i][j] * rhs.v[j];
	}

	return result;
}

/**************************************************************************
*!
\fn     Matrix4::operator+

\brief Matrix Addition


\param  const Matrix4 & rhs


\return Matrix4


*
**************************************************************************/
Matrix4 Matrix4::operator+(const Matrix4 & rhs) const
{
	Matrix4 result;

	for (int i = 0; i < 16; i++)
		result.v[i] = v[i] + rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Matrix4::operator-

\brief Matrix Substraction


\param  const Matrix4 & rhs


\return Matrix4


*
**************************************************************************/
Matrix4 Matrix4::operator-(const Matrix4 & rhs) const
{
	Matrix4 result;

	for (int i = 0; i < 16; i++)
		result.v[i] = v[i] - rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Matrix4::operator*

\brief Matrix Multiplication


\param  const Matrix4 & rhs


\return Matrix4


*
**************************************************************************/
Matrix4 Matrix4::operator*(const Matrix4 & rhs) const
{
	Matrix4 result;

	for (int i = 0; i < 4; i++) //row
		for (int j = 0; j < 4; j++) //col
			for (int k = 0; k < 4; k++)
				result.m[i][j] += m[i][k] * rhs.m[k][j];

	return result;
}

/**************************************************************************
*!
\fn     Matrix4::operator+=

\brief Matrix Addition, modifying the original


\param  const Matrix4 & rhs


\return Matrix4 &


*
**************************************************************************/
Matrix4 & Matrix4::operator+=(const Matrix4 & rhs)
{
	for (int i = 0; i < 16; i++)
		v[i] += rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Matrix4::operator-=

\brief Matrix Substraction, modifying the original


\param  const Matrix4 & rhs


\return Matrix4 &


*
**************************************************************************/
Matrix4 & Matrix4::operator-=(const Matrix4 & rhs)
{
	for (int i = 0; i < 16; i++)
		v[i] -= rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Matrix4::operator*=

\brief Matrix Multiplication, modifying the original


\param  const Matrix4 & rhs


\return Matrix4 &


*
**************************************************************************/
Matrix4 & Matrix4::operator*=(const Matrix4 & rhs)
{
	*this = *this * rhs;

	return *this;
}

/**************************************************************************
*!
\fn     Matrix4::operator*

\brief Scale the entire matrix by a float


\param  const float rhs


\return Matrix4


*
**************************************************************************/
Matrix4 Matrix4::operator*(const float rhs) const
{
	Matrix4 result;

	for (int i = 0; i < 16; i++)
		result.v[i] = rhs * v[i];

	return result;
}

/**************************************************************************
*!
\fn     Matrix4::operator/

\brief  Divide the entire matrix by a float


\param  const float rhs


\return Matrix4


*
**************************************************************************/
Matrix4 Matrix4::operator/(const float rhs) const
{
	Matrix4 result;

	for (int i = 0; i < 16; i++)
		result.v[i] = v[i] / rhs;

	return result;
}

/**************************************************************************
*!
\fn     Matrix4::operator*=

\brief Scale the entire original matrix by a float


\param  const float rhs


\return Matrix4 &


*
**************************************************************************/
Matrix4 & Matrix4::operator*=(const float rhs)
{
	for (int i = 0; i < 16; i++)
		v[i] *= rhs;

	return *this;

}

/**************************************************************************
*!
\fn     Matrix4::operator/=

\brief dIVIDE the entire original matrix by a float


\param  const float rhs


\return Matrix4 &


*
**************************************************************************/
Matrix4 & Matrix4::operator/=(const float rhs)
{
	for (int i = 0; i < 16; i++)
		v[i] = v[i] / rhs;

	return *this;
}

/**************************************************************************
*!
\fn     Matrix4::operator==

\brief True if two Matrices are equal, false if not


\param  const Matrix4 & rhs


\return bool


*
**************************************************************************/
bool Matrix4::operator==(const Matrix4 & rhs) const
{
	for (int i = 0; i < 16; i++)
		if (!isEqual(v[i], rhs.v[i]))
			return false;

	return true;
}

/**************************************************************************
*!
\fn     Matrix4::operator!=

\brief False if two Matrices are equal, true if not


\param  const Matrix4 & rhs


\return bool


*
**************************************************************************/
bool Matrix4::operator!=(const Matrix4 & rhs) const
{
	for (int i = 0; i < 16; i++)
		if (!isEqual(v[i], rhs.v[i]))
			return true;

	return false;
}

/**************************************************************************
*!
\fn     Matrix4::Zero

\brief Zeroes out the entire matrix


\param  void


*
**************************************************************************/
void Matrix4::Zero(void)
{
	for (int i = 0; i < 16; i++)
		v[i] = 0;
}

/**************************************************************************
*!
\fn     Matrix4::Identity

\brief Builds the identity matrix


\param  void


*
**************************************************************************/
void Matrix4::Identity(void)
{
	/*for (int i = 0; i < 4; i++)
		for (int j = 0; j < 4; j++)
		{
			if (i == j)
				m[i][j] = 1;
			else
				m[i][j] = 0;
		}*/
	*this = Matrix4(
		1, 0, 0, 0,
		0, 1, 0, 0,
		0, 0, 1, 0,
		0, 0, 0, 1);
}

//~
void Matrix4::BuildScale(float x, float y, float z)
{
	m[0][0] = x;
	m[1][1] = y;
	m[2][2] = z;
	m[3][3] = 1;
}

//~
void Matrix4::BuildRotation(float angle_x, float angle_y, float angle_z)
{
	Matrix4 x, y, z;

	x.BuildRotation_x(angle_x);
	y.BuildRotation_y(angle_y);
	z.BuildRotation_z(angle_z);

	*this = x * y * z;	
}

//~
void Matrix4::BuildRotation_x(float angle)
{
	Identity();

	m[1][1] = cos(angle);
	m[1][2] = -sin(angle);
	m[2][1] = sin(angle);
	m[2][2] = cos(angle);
}

//~
void Matrix4::BuildRotation_y(float angle)
{
	Identity();

	m[0][0] = cos(angle);
	m[0][2] = sin(angle);
	m[2][0] = -sin(angle);
	m[2][2] = cos(angle);
}

//~
void Matrix4::BuildRotation_z(float angle)
{
	Identity();

	m[0][0] = cos(angle);
	m[0][1] = -sin(angle);
	m[1][0] = sin(angle);
	m[1][1] = cos(angle);
}

//~
void Matrix4::BuildTranslation(float x, float y, float z)
{
	Identity();

	m[0][3] = x;
	m[1][3] = y;
	m[2][3] = z;
}

//~
void Matrix4::BuildPerspectiveProj(float d)
{
	Zero();

	for (int i = 0; i < 3; i++)
		m[i][i] = d;

	m[3][2] = -1;
}

//~
void Matrix4::BuildWindowToViewport(float Window_W, float Window_H, float Viewport_W, float Viewport_H)
{
	Identity();

	m[0][0] = Viewport_W / Window_W;
	m[1][1] = -Viewport_H / Window_H;
	m[0][3] = Viewport_W / 2.f;
	m[1][3] = Viewport_H / 2.f;
}
