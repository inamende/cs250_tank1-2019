/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: Point4.cpp
Purpose: Math Library
Language: C++
Platform: PC, Windows 7
Project: ignacio.mendezona@digipen.edu ,cs250 Math Library
class CS250
Author: Ignacio Mendezona , ignacio.mendezona@digipen.edu
Date: Wed Jan 24 11:58:18 2018
----------------------------------------------------------------------------------------------------------*/

#include "Point4.h"
#include "MathUtilities.h"

/**************************************************************************
*!
\fn     Point4::Point4

\brief Default constructor, sets x,y,z to zero and w to the defined value


\param  void


*
**************************************************************************/
Point4::Point4(void)
{
	for (int i = 0; i < 4; i++)
		v[i] = 0;

	v[3] = 1;
}

/**************************************************************************
*!
\fn     Point4::Point4

\brief Copy constructor, copies every component from the other Point4


\param  const Point4 & rhs


*
**************************************************************************/
Point4::Point4(const Point4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] = rhs.v[i];
}


/**************************************************************************
*!
\fn     Point4::Point4

\brief Non-Default constructor, self-explanatory


\param  All vector components


*
**************************************************************************/
Point4::Point4(float xx, float yy, float zz, float ww)
{
	v[0] = xx;
	v[1] = yy;
	v[2] = zz;
	v[3] = ww;
}

/**************************************************************************
*!
\fn     Point4::operator=

\brief Assignment operator, copies every component from the other Point4


\param  const Point4 & rhs


\return Point4 &


*
**************************************************************************/
Point4 & Point4::operator=(const Point4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] = rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Point4::operator-

\brief Unary negation operator, negates every component and returns a copy


\param  void


\return Point4


*
**************************************************************************/
Point4 Point4::operator-(void) const
{
	Point4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = -v[i];

	return result;
}

/**************************************************************************
*!
\fn     Point4::operator-

\brief Binary subtraction operator, Subtract two Point4s and you get a Vector4


\param  const Point4 & rhs


\return Vector4


*
**************************************************************************/
Vector4 Point4::operator-(const Point4 & rhs) const
{
	Vector4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = v[i] - rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Point4::operator+

\brief Point Addition


\param  const Vector4 & rhs


\return Point4


*
**************************************************************************/
Point4 Point4::operator+(const Vector4 & rhs) const
{
	Point4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = v[i] + rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Point4::operator-

\brief Point Substraction


\param  const Vector4 & rhs


\return Point4


*
**************************************************************************/
Point4 Point4::operator-(const Vector4 & rhs) const
{
	Point4 result;

	for (int i = 0; i < 4; i++)
		result.v[i] = v[i] - rhs.v[i];

	return result;
}

/**************************************************************************
*!
\fn     Point4::operator+=

\brief Point addition midifying original


\param  const Vector4 & rhs


\return Point4 &


*
**************************************************************************/
Point4 & Point4::operator+=(const Vector4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] += rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Point4::operator-=

\brief Point Substraction midifying original


\param  const Vector4 & rhs


\return Point4 &


*
**************************************************************************/
Point4 & Point4::operator-=(const Vector4 & rhs)
{
	for (int i = 0; i < 4; i++)
		v[i] -= rhs.v[i];

	return *this;
}

/**************************************************************************
*!
\fn     Point4::operator==

\brief True if two points are equal, false if not


\param  const Point4 & rhs


\return bool


*
**************************************************************************/
bool Point4::operator==(const Point4 & rhs) const
{
	for (int i = 0; i < 3; i++)
		if (!isEqual(v[i], rhs.v[i]))
			return false;

	return true;
}

/**************************************************************************
*!
\fn     Point4::operator!=

\brief False if two points are equal, true if not


\param  const Point4 & rhs


\return bool


*
**************************************************************************/
bool Point4::operator!=(const Point4 & rhs) const
{
	for (int i = 0; i < 4; i++)
		if (!isEqual(v[i], rhs.v[i]))
			return true;

	return false;
}

/**************************************************************************
*!
\fn     Point4::Zero

\brief Set all point components to 0s (the last to 1)


\param  void


*
**************************************************************************/
void Point4::Zero(void)
{
	for (int i = 0; i < 3; i++)
		v[i] = 0.0f;

	v[3] = 1.0f;
}

