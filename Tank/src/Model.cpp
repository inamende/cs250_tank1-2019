/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: Model.cpp
Purpose: This file contains all the functions from model.h
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#include "Model.h"
#include "CS250Parser.h"
#include "Matrix4.h"
#include "Utilities.h"

#include <iostream>

bool Model::renderLines = false;
//~
void Model::Draw()
{
	Matrix4 translation, rotation, scale, transformation, projection, windowToViewport;
	Model* tempParent = parent;
	translation.BuildTranslation(translation_.x, translation_.y, translation_.z);
	rotation.BuildRotation(rotation_.x, rotation_.y, rotation_.z);
	scale.BuildScale(scale_.x, scale_.y, scale_.z);

	//Model To World 
	transformation = translation * rotation * scale;

	while (tempParent != nullptr)
	{
		translation.BuildTranslation(tempParent->translation_.x, tempParent->translation_.y, tempParent->translation_.z);
		rotation.BuildRotation(tempParent->rotation_.x, tempParent->rotation_.y, tempParent->rotation_.z);

		transformation = translation * rotation * transformation;

		tempParent = tempParent->parent;
	}

	//Perspective Projection
	projection.BuildPerspectiveProj(CS250Parser::focal);

	windowToViewport.BuildWindowToViewport((CS250Parser::right - CS250Parser::left), (CS250Parser::top - CS250Parser::bottom), WIDTH, HEIGHT);

	for (unsigned i = 0; i < CS250Parser::faces.size(); i++)
	{
		Point4 triangle[3] = {};
		Color c;
		c = CS250Parser::colors[i];

		for (int j = 0; j < 3; j++)
		{
			triangle[j] = CS250Parser::vertices[CS250Parser::faces[i].indices[j]];
			//triangle[j].color_ =;

			// Transform Vertices
			triangle[j] = transformation * triangle[j];  // Model to world
			triangle[j] = projection * triangle[j];    // Perspective projection

			//Perpective division
			triangle[j].x /= triangle[j].w;
			triangle[j].y /= triangle[j].w;
			triangle[j].z /= triangle[j].w;
			triangle[j].w /= triangle[j].w;

			//Window to viewport
			triangle[j] = windowToViewport * triangle[j];

			//std::cout << "Start" << std::endl; 
			//std::cout << "End" << std::endl;
		}

		if (renderLines)
		{
			DrawLine(triangle[0], triangle[1], c);
			DrawLine(triangle[1], triangle[2], c);
			DrawLine(triangle[2], triangle[0], c);
		}
		else DrawTriangle(triangle[2], triangle[1], triangle[0], c);
	}
}