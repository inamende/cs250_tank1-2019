/* ---------------------------------------------------------------------------------------------------------
Copyright (C) 2018 DigiPen Institute of Technology.
Reproduction or disclosure of this file or its contents without the prior written
consent of DigiPen Institute of Technology is prohibited.
File Name: main.cpp
Purpose: All the "gameplay" code
Language: C++
Platform: Windows7
Project: Assingment 2, Tank
Author: Ignacio Mendezona, ignacio.mendezona
Creation date: 
----------------------------------------------------------------------------------------------------------*/
#include "Utilities.h"
#include "Rasterizer.h"
#include "DrawTriangle.h"
#include "DrawLine.h"
#include "CS250Parser.h"
#include "Model.h"
#include "Matrix4.h"
#include <iostream>

int winID;

//Creation of the parts of the tank
Model body{};
Model wheels[4];
Model turret{};
Model gun{};

void keyboard(unsigned char key, int x, int y)
{
	switch (key)
	{
		case VK_ESCAPE:
			delete [] FrameBuffer::buffer;
			glutDestroyWindow(winID);
			exit(0);
			break;

		case 'd': //body left
			body.rotation_.y += 0.1f;
			break;

		case 'a': //body right
			body.rotation_.y -= 0.1f;
			break;

		case ' ': //move forward
			body.translation_.z += cos(body.rotation_.y);
			body.translation_.x += sin(body.rotation_.y);

			//Wheel rotation
			for (size_t i = 0; i < 4; i++)
				wheels[i].rotation_.x += 0.1f;

			break;
		
		case 'q': //turret left
			turret.rotation_.y -= 0.1f;
			break;

		case 'e': //turret righ 
			turret.rotation_.y += 0.1f;
			break;

		case 'r': //gun up
			gun.rotation_.x += 0.1f;
			gun.translation_.y =  -sin(gun.rotation_.x) * (gun.scale_.z / 2);
			gun.translation_.z =  cos(gun.rotation_.x) * (gun.scale_.z / 2) + 12.5f;
			break;

		case 'f': //gun down
			gun.rotation_.x -= 0.1f;
			gun.translation_.y = -sin(gun.rotation_.x) * (gun.scale_.z / 2);
			gun.translation_.z = cos(gun.rotation_.x) * (gun.scale_.z / 2) + 12.5f;
			break;

		case '2': //Render lines
			Model::renderLines = !Model::renderLines;
			break;
	}
}

void mouse(int button, int state, int x, int y)
{
	/*switch (button)
	{
	}*/
}

void mouseMove(int x, int y)
{
}

void loop(void)
{
	glutPostRedisplay();
}

void render(void)
{
	FrameBuffer::Clear(255, 255, 255);

	body.Draw();

	for (size_t i = 0; i < 4; i++)
	wheels[i].Draw();

	turret.Draw();
	gun.Draw();

	glDrawPixels(WIDTH, HEIGHT, GL_RGB, GL_UNSIGNED_BYTE, FrameBuffer::buffer);
	glutSwapBuffers();
}

void init(void)
{
	FrameBuffer::Init(WIDTH, HEIGHT);

	//Initialize everything here
	CS250Parser::LoadDataFromFile();

	//body setup
	body.translation_.z = -100;
	body.scale_ = { 30,25,80 };

	//wheels setup
	wheels[0].translation_ = { 17.5, -12.5, -25 };
	wheels[1].translation_ = { -17.5, -12.5, -25 };
	wheels[2].translation_ = { 17.5, -12.5, 25 };
	wheels[3].translation_ = { -17.5, -12.5, 25 };

	for (size_t i = 0; i < 4; i++)
	{
		wheels[i].scale_ = { 5,20,20 };
		wheels[i].parent = &body;
	}

	//turret setup
	turret.translation_ = { 0,20,0 };
	turret.scale_ = { 25,15,25 };
	turret.parent = &body;

	//gun setup
	gun.translation_ = { 0,0,32.5};
	gun.scale_ = { 5,5,40 };
	gun.parent = &turret;
}

int main (int argc, char **argv)
{
	glutInit(&argc, argv);
	glutInitDisplayMode(GLUT_RGBA | GLUT_DOUBLE);
	glutInitWindowPosition(100, 100);
	glutInitWindowSize(WIDTH, HEIGHT);
	winID = glutCreateWindow("CS250");

	glClearColor(0, 0, 0, 1);

	glutKeyboardFunc(keyboard);
	glutDisplayFunc(render);
	glutIdleFunc(loop);
	glutMouseFunc(mouse);
	glutMotionFunc(mouseMove);

	init();

	glutMainLoop();

	return 0;
}